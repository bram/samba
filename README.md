# Anonymous Samba Data Drop Demo

Zoiets:
```
docker compose build
docker compose up -d
```

Test (zorg voor genoeg backslashes, want daar houdt samba/windows van):
```

smbclient -p 1139 -N \\\\localhost\\share
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Fri Apr  5 21:26:00 2024
  ..                                  D        0  Fri Apr  5 21:25:44 2024
  .gitkeep                            H        0  Fri Apr  5 21:11:45 2024
  foo                                 D        0  Fri Apr  5 21:26:00 2024

		240784932 blocks of size 1024. 42244692 blocks available
smb: \> mkdir bar
smb: \> ls
  .                                   D        0  Fri Apr  5 21:26:38 2024
  ..                                  D        0  Fri Apr  5 21:25:44 2024
  bar                                 D        0  Fri Apr  5 21:26:38 2024
  .gitkeep                            H        0  Fri Apr  5 21:11:45 2024
  foo                                 D        0  Fri Apr  5 21:26:00 2024

		240784932 blocks of size 1024. 42244380 blocks available
```

:party;
